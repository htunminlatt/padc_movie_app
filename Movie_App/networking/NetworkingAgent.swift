//
//  NetworkingAgent.swift
//  Movie_App
//
//  Created by Admin on 10/07/2021.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

struct MovieNetworkAgent {
    private init() { return }
    
    static let shared = MovieNetworkAgent()
    
    func getMovieTariler(id: Int,completion: @escaping (MovieDBResult<MovieTrailerResopnse>) -> Void) {
        
        AF.request(MDBEndpoint.movieTrailer(id))
            .responseDecodable(of: MovieTrailerResopnse.self) { response in
                switch response.result {
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                case .success(let data):
                    completion(.success(data))
                }
            }
       }
    
    func getSearchMovie(query: String ,page: Int,completion: @escaping (MovieDBResult<MovieListResponse>) -> Void) {
        
        let url = URL(string: "https://api.themoviedb.org/3/search/movie?query=\(query)&page=\(page)&api_key=46ece7256a33d62d8ea238b215965aea")!

        AF.request(url)
            .responseDecodable(of: MovieListResponse.self) { response in
                switch response.result {
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                case .success(let data):
                    completion(.success(data))
                }
            }
       }
    
    func getMovieDetail(id: Int, completion: @escaping (MovieDBResult<MovieDetailResopnse>)-> Void) {
        AF.request(MDBEndpoint.movieDetail(id))
            .responseDecodable(of: MovieDetailResopnse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getCoomingSoonMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void) {
        AF.request(MDBEndpoint.movieList("comingsoon"))
            .responseDecodable(of: NowPlayingMovieResopnse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getNowPlayingMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void) {
        AF.request(MDBEndpoint.movieList("current"))
            .responseDecodable(of: NowPlayingMovieResopnse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    

    func loginUser(username: String,password: String,requestToken: String,completion: @escaping (MovieDBResult<loginSuccess>) -> Void) {
        struct userData: Encodable {
            let username: String?
            let password: String?
            let request_token: String?
        }
        
        let reqObject = userData(username: username, password: password,request_token:requestToken)
        
        AF.request(MDBEndpoint.loginUser,method: .post,parameters: reqObject)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: loginSuccess.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    func registerWithEmail(name: String,email: String,phone: String,password: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct userData: Encodable {
            let name: String?
            let email: String?
            let phone: String?
            let password: String?
        }
        
        let reqObject = userData(name: name, email: email, phone: phone, password: password)
        
        AF.request(MDBEndpoint.registerEmail,method: .post,parameters: reqObject)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    func registerWithFacebook(name: String,email: String,phone: String,password: String,fbaccessToken: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct userData: Encodable {
            let name: String?
            let email: String?
            let phone: String?
            let password: String?
            let fbaccessToken: String
            
            enum CodingKeys: String, CodingKey {
                case fbaccessToken = "facebook-access-token"
                case name
                case email
                case phone
                case password
            }
        }
        
        let reqObject = userData(name: name, email: email, phone: phone, password: password, fbaccessToken: fbaccessToken)
        
        AF.request(MDBEndpoint.registerEmail,method: .post,parameters: reqObject)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    
    func registerWithGoogle(name: String,email: String,phone: String,password: String,googleAccessToken: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct userData: Encodable {
            let name: String?
            let email: String?
            let phone: String?
            let password: String?
            let googleAccessToken: String
            
            enum CodingKeys: String, CodingKey {
                case googleAccessToken = "google-access-token"
                case name
                case email
                case phone
                case password
            }
        }
        
        let reqObject = userData(name: name, email: email, phone: phone, password: password, googleAccessToken: googleAccessToken)
        
        AF.request(MDBEndpoint.registerEmail,method: .post,parameters: reqObject)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }


    func loginWithEmail(email: String,password: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct userData: Encodable {
            let email: String?
            let password: String?
        }
        
        let reqObject = userData(email: email, password: password)
        
        AF.request(MDBEndpoint.loginWithEmail,method: .post,parameters: reqObject)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    func loginWithFacebook(access_token: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct token: Encodable {
            let access_token: String?
            
            enum CodingKeys: String, CodingKey {
                case access_token = "access-token"
            }
        }
        
        let reqObject = token(access_token: access_token)
        
        AF.request(MDBEndpoint.loginWithFacebook,method: .post,parameters: reqObject)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    func loginWithGoogle(access_token: String,completion: @escaping (MovieDBResult<RegisterDataResopnse>) -> Void) {
        struct token: Encodable {
            let access_token: String?
            
            enum CodingKeys: String, CodingKey {
                case access_token = "access-token"
            }
        }
        
        let reqObject = token(access_token: access_token)
        
        AF.request(MDBEndpoint.loginGoogle,method: .post,parameters: reqObject)
            .responseDecodable(of: RegisterDataResopnse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
    }
    
    func fetchProfileData(completion: @escaping (MovieDBResult<UserProfileDataResopnse>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.profile,method: .get,headers: headers)
            .responseDecodable(of: UserProfileDataResopnse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func logout(completion: @escaping (MovieDBResult<Logout>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.logout,method: .post,headers: headers)
            .responseDecodable(of: Logout.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getCinemaTimeSlot(date: String,completion: @escaping (MovieDBResult<CinemaDayTimeSlotResopnse>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.cinemaTimeSlot(date),method: .get,headers: headers)
            .responseDecodable(of: CinemaDayTimeSlotResopnse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getMovieSeat(bookingDate: String,id: Int,completion: @escaping (MovieDBResult<MovieSeatResponse>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.movieSeat(id, bookingDate),method: .get,headers: headers)
            .responseDecodable(of: MovieSeatResponse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getSanckList(completion: @escaping (MovieDBResult<SnackListResponse>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.snackList,method: .get,headers: headers)
            .responseDecodable(of: SnackListResponse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func getPaymentMethod(completion: @escaping (MovieDBResult<PaymentMethodResponse>)-> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        AF.request(MDBEndpoint.paymentMethod,method: .get,headers: headers)
            .responseDecodable(of: PaymentMethodResponse.self) { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
            }
    }
    
    func createNewCard(cardNumber:String,cardHolder: String,date: String,cvc: String, completion: @escaping (MovieDBResult<CreateCardResponse>) -> Void) {
        struct cardData: Encodable {
            let card_number: String?
            let card_holder: String?
            let expiration_date: String?
            let cvc:String?
        }
        
        let reqObject = cardData(card_number: cardNumber, card_holder: cardHolder, expiration_date: date, cvc: cvc)
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? "")]

        
        AF.request(MDBEndpoint.createCard,method: .post,parameters: reqObject,headers: headers)
            .responseDecodable(of: CreateCardResponse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    func checkOut(movieId: Int,timeslot_id:Int,seat_number: String,booking_date: String,card_id: Int,snackArray: [snack], completion: @escaping (MovieDBResult<CheckOutSuccessResponse>) -> Void) {

        struct checkOutData: Encodable {
            let cinema_day_timeslot_id: Int
            let booking_date: String
            let seat_number: String
            let card_id:Int
            let movie_id:Int
            let snacks: [snack]
            
            enum CodingKeys: String, CodingKey {
                case cinema_day_timeslot_id
                case booking_date
                case card_id
                case movie_id
                case snacks
                case seat_number
            }
        }
        
      
        let reqObject = checkOutData(cinema_day_timeslot_id: timeslot_id, booking_date: booking_date, seat_number: seat_number, card_id: card_id, movie_id: movieId, snacks: snackArray)
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Defaults[\.token] ?? ""),.contentType("application/json")]

        AF.request(MDBEndpoint.checkOut,method: .post,parameters: reqObject,encoder: JSONParameterEncoder.default, headers: headers)
            .responseDecodable(of: CheckOutSuccessResponse.self) {response in
                switch response.result{
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
                }
                
            }
        }
    
    //upload image with multipart form data
    func upladImages(files: [MultiPartFileData],completion: @escaping (MovieDBResult<MovieDetailResopnse>) -> Void) {
        AF.upload(multipartFormData: { (multipartFormData) in
            files.forEach { (item) in
                multipartFormData.append(item.data, withName: item.formfieldName, fileName: item.filename, mimeType: item.mineType)
            }
        }, to: MDBEndpoint.checkOut)
        .uploadProgress(closure: { (progerss) in
            print("...\(progerss)")
        })
        .responseDecodable(of: MovieDetailResopnse.self) { (response) in
            switch response.result{
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.fail(handleError(response, error, MDBCommonResponseError.self)))
            }
        }
    }
    
}



enum MovieDBResult<T> {
    case success(T)
    case fail(String)
}


fileprivate func handleError<T, E: MDBErrorModel>(
        _ response : DataResponse<T, AFError>,
        _ error: (AFError),
        _ errorBodyType : E.Type) -> String {

        var respBody : String = ""
        var serverErrorMessage : String?

        var errorBody: E?
    if let respData = response.data {
        respBody = String(data: respData, encoding: .utf8) ?? "empty response body"
        errorBody = try? JSONDecoder().decode(errorBodyType,from: respData)
        serverErrorMessage = errorBody?.message
    }


    let respCode : Int = response.response?.statusCode ?? 0

    let sourcePath = response.request?.url?.absoluteString ?? "no url"

    print (
            """
             ===================
            URL
            -> \(sourcePath)

            Status
            -> \(respCode)

            Body
            -> \(respBody)

            Underlying Error
            -> \(error.underlyingError)

            Error Description
            -> \(error.errorDescription)
            =====================
            """
        )

return serverErrorMessage ?? error.errorDescription ?? "undefined"
}

protocol  MDBErrorModel: Decodable {
var message : String { get }
}

class MDBCommonResponseError : MDBErrorModel {
var message: String {
    return statusMessage
}

let statusMessage : String
let statusCode : Int

enum CodingKeys : String, CodingKey {
    case statusMessage = "message"
    case statusCode = "code"
}
}

