//
//  MovieEndPoint.swift
//  Movie_App
//
//  Created by Admin on 10/07/2021.
//

import Foundation
import Alamofire

enum MDBEndpoint: URLConvertible {
    case getRequestToken
    case loginUser
    case nowPlayingMovie
    case coomingSoonMovie
    case movieDetail(_ id: Int)
    case searchMovie(_ query: String,_ page:Int)
    case movieTrailer(_ id: Int)
    
    case registerEmail
    case loginWithEmail
    case loginWithFacebook
    case loginGoogle
    case movieList(_ status: String)
    case profile
    case logout
    
    case cinemaTimeSlot(_ date: String)
    case movieSeat(_ timeSlotId: Int,_ bookingDate: String)
    case snackList
    case paymentMethod
    case createCard
    case checkOut
    
    private var baseUrl: String {
        return Constants.url
    }
    
    func asURL() throws -> URL {
        return url
    }
    
    var url: URL {
        let urlComponents = NSURLComponents(string: baseUrl.appending(apiPath))
        if urlComponents?.queryItems == nil {
            urlComponents?.queryItems = []
        }
        
        
        return urlComponents!.url!
    }
    
    // api url
    private var apiPath: String {
        switch self {
        case .getRequestToken:
            return "/authentication/token/new"
        case .loginUser:
            return "/authentication/token/validate_with_login"
        case .nowPlayingMovie:
            return "/movie/now_playing"
        case .coomingSoonMovie:
            return "/movie/upcoming"
        case .movieDetail(let id):
            return "/api/v1/movies/\(id)"
            
        case .searchMovie(let query, let page):
            return "/search/movie?query=\(query)&page=\(page)"
        case .movieTrailer(let id):
            return "/movie/\(id)/videos"
            
        case .registerEmail:
            return "/api/v1/register"
        case .loginWithEmail:
            return "/api/v1/email-login"
        case .loginWithFacebook:
            return "/api/v1/facebook-login"
        case .loginGoogle:
            return "/api/v1/google-login"

        case .movieList(let status):
            return "/api/v1/movies?status=\(status)"
        case .profile:
            return "/api/v1/profile"
        case .logout:
            return "/api/v1/logout"
            
        case .cinemaTimeSlot(let date):
            return "/api/v1/cinema-day-timeslots?date=\(date)"
        case .movieSeat(let id, let date):
            return "/api/v1/seat-plan?cinema_day_timeslot_id=\(id)&booking_date=\(date)"
        case .snackList:
            return "/api/v1/snacks"
        case .paymentMethod:
            return "/api/v1/payment-methods"
        case .createCard:
            return "/api/v1/card"
        case .checkOut:
            return "/api/v1/checkout"
    }
    
   }
    
}
