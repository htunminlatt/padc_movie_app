//
//  MovieType.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import Foundation
import UIKit

enum MovieType: Int {
    case NOWPLAYING_MOVIE = 0
    case COOMINGSOON_MOVIE = 1
    
}
