//
//  CheckOutModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation
protocol CheckOutModel {
    func checkOut(movieId: Int,timeslot_id:Int,seat_number: String,booking_date: String,card_id: Int,snackArray: [snack], completion: @escaping (MovieDBResult<CheckOutSuccessResponse>) -> Void)
}

class CheckOutModelImpl: BaseModel,CheckOutModel {
   
    static let shared = CheckOutModelImpl()
    private override init() {}
    
    func checkOut(movieId: Int, timeslot_id: Int, seat_number: String, booking_date: String, card_id: Int, snackArray: [snack], completion: @escaping (MovieDBResult<CheckOutSuccessResponse>) -> Void) {
        networkAgent.checkOut(movieId: movieId, timeslot_id: timeslot_id, seat_number: seat_number, booking_date: booking_date, card_id: card_id, snackArray: snackArray, completion: completion)
    }
  
}
