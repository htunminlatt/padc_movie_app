//
//  SnackModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation

protocol SnackModel {
    func getSanckList(completion: @escaping (MovieDBResult<SnackListResponse>)-> Void )
    func getPaymentMethod(completion: @escaping (MovieDBResult<PaymentMethodResponse>)-> Void )
}

class SnackModelImpl: BaseModel,SnackModel {
   
    static let shared = SnackModelImpl()
    private override init() {}
  
    func getSanckList(completion: @escaping (MovieDBResult<SnackListResponse>) -> Void) {
        networkAgent.getSanckList(completion: completion)
    }
    
    func getPaymentMethod(completion: @escaping (MovieDBResult<PaymentMethodResponse>) -> Void) {
        networkAgent.getPaymentMethod(completion: completion)
    }
   
}
