//
//  MovieModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation

protocol MovieModel {
    func getNowPlayingMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void )
    func getCoomingSoonMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void )
    func fetchProfileData(completion: @escaping (MovieDBResult<UserProfileDataResopnse>)-> Void)
}

class MovieModelImpl: BaseModel,MovieModel {
    
    static let shared = MovieModelImpl()
    private override init() {}
    
    func getNowPlayingMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void ){
        networkAgent.getNowPlayingMovie(completion: completion)
    }
    func getCoomingSoonMovie(completion: @escaping (MovieDBResult<NowPlayingMovieResopnse>)-> Void ){
        networkAgent.getCoomingSoonMovie(completion: completion)
    }
    func fetchProfileData(completion: @escaping (MovieDBResult<UserProfileDataResopnse>) -> Void) {
        networkAgent.fetchProfileData(completion: completion)
    }
}
