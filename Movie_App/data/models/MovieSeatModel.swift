//
//  MovieSeatModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation
protocol MovieSeatModel {
    func getMovieSeat(bookingDate: String,id: Int,completion: @escaping (MovieDBResult<MovieSeatResponse>)-> Void )
}

class MovieSeatImpl: BaseModel,MovieSeatModel {
   
    static let shared = MovieSeatImpl()
    private override init() {}
  
    func getMovieSeat(bookingDate: String, id: Int, completion: @escaping (MovieDBResult<MovieSeatResponse>) -> Void) {
        networkAgent.getMovieSeat(bookingDate: bookingDate, id: id, completion: completion)
    }
}
