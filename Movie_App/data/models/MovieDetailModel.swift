//
//  MovieDetailModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation

protocol MovieDetailModel {
    func getMovieDetail(id: Int,completion: @escaping (MovieDBResult<MovieDetailResopnse>)-> Void )
}

class MovieDetailImpl: BaseModel,MovieDetailModel {
   
    static let shared = MovieDetailImpl()
    private override init() {}
    
    func getMovieDetail(id: Int, completion: @escaping (MovieDBResult<MovieDetailResopnse>) -> Void) {
        networkAgent.getMovieDetail(id: id, completion: completion)
    }
    
    
    
}
