//
//  MovieTimeModel.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation

protocol MovieTimeModel {
    func getCinemaTimeSlot(date: String,completion: @escaping (MovieDBResult<CinemaDayTimeSlotResopnse>)-> Void )
}

class MovieTimeImpl: BaseModel,MovieTimeModel {
   
    static let shared = MovieTimeImpl()
    private override init() {}
  
    func getCinemaTimeSlot(date: String, completion: @escaping (MovieDBResult<CinemaDayTimeSlotResopnse>) -> Void) {
        networkAgent.getCinemaTimeSlot(date: date, completion: completion)
    }
  
}
