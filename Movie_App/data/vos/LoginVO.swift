//
//  LoginVO.swift
//  Movie_App
//
//  Created by Admin on 24/07/2021.
//

import Foundation

class LoginVO {
    static var shared = LoginVO()
    var simpleLogin : Bool = false
    var facebookLogin: Bool = false
    var googleLogin: Bool = false
    
    var email: String = ""
    var username: String = ""
}
