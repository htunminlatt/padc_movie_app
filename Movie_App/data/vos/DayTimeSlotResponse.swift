// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cinemaDayTimeSlotResopnse = try? newJSONDecoder().decode(CinemaDayTimeSlotResopnse.self, from: jsonData)

import Foundation

// MARK: - CinemaDayTimeSlotResopnse
struct CinemaDayTimeSlotResopnse: Codable {
    let code: Int?
    let message: String?
    let data: [ResponseData]?
}

// MARK: - Datum
struct ResponseData: Codable {
    let cinemaID: Int?
    let cinema: String?
    let timeslots: [Timeslot]?

    enum CodingKeys: String, CodingKey {
        case cinemaID = "cinema_id"
        case cinema, timeslots
    }
}

// MARK: - Timeslot
struct Timeslot: Codable {
    let cinemaDayTimeslotID: Int?
    let startTime: String?
    let isSelected: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case cinemaDayTimeslotID = "cinema_day_timeslot_id"
        case startTime = "start_time"
    }
    
    
    func convertToTickVO() -> TicketVO {
        return TicketVO(time: startTime ?? "", isSelected: isSelected, id: cinemaDayTimeslotID ?? 0)
    }
}
