//
//  TicketVO.swift
//  Movie_App
//
//  Created by Sai Xtun on 01/02/2021.
//

import Foundation

class TicketVO {
    var time : String = "6 AM"
    var isSelected : Bool = false
    var id: Int = 0
    
    init(time: String,isSelected: Bool,id: Int) {
        self.time = time
        self.isSelected = isSelected
        self.id = id
    }
}

class CalendarVO {
    var day : String = ""
    var dayNumber: Int = 0
    var isSelected : Bool = false
    
    init(day: String,dayNumber: Int,isSelected: Bool) {
        self.day = day
        self.dayNumber = dayNumber
        self.isSelected = isSelected
    }
}


