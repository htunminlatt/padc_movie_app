//
//  authentication.swift
//  Movie_App
//
//  Created by Admin on 10/07/2021.
//

import Foundation

struct loginRequest: Codable {
    let username: String
    let password: String
    let requestToken: String
    
    enum CodingKeys: String,CodingKey {
        case username
        case password
        case requestToken = "request_token"
    }
    
}

struct requestTokenForLogin: Codable {
    let success: Bool?
    let expires_at: String?
    let request_token: String?
    
}


struct loginSuccess:Codable {
    let success: Bool?
    let expires_at: String?
    let request_token: String?
    
}

struct loginFail:Codable {
    let success: Bool?
    let statusCode: Int?
    let statusMessage: String?
    
    enum CodingKeys: String,CodingKey {
        case success
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
