// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let snackListResponse = try? newJSONDecoder().decode(SnackListResponse.self, from: jsonData)

import Foundation

// MARK: - SnackListResponse
struct SnackListResponse: Codable {
    let code: Int?
    let message: String?
    let data: [SnackData]?
}

// MARK: - Datum
struct SnackData: Codable {
    let id: Int?
    let name, datumDescription: String?
    let price: Int?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case datumDescription = "description"
        case price, image
    }
}
