//
//  MovieGenre.swift
//  Movie_App
//
//  Created by Admin on 10/07/2021.
//

import Foundation

// MARK: - GenreMovieLists
struct GenreMovieLists: Codable {
    let genres: [MovieGenre]?
}

// MARK: - Genre
public struct MovieGenre: Codable {
   public let id: Int?
    public let name: String?
    
    enum CodingKeys: String,CodingKey {
        case id
        case name = "name"
    }
    
}
