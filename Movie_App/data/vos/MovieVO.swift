//
//  MovieVO.swift
//  Movie_App
//
//  Created by MML on 01/06/2021.
//

import Foundation

class MovieSeatVO {
    var title: String
    var type: String
    var price: Int
    var isSelect: Bool
    var seatNumber: String
    var seatId: Int
    
    func isMovieSeatAvailable()-> Bool {
        return type == SEAT_TYPE_AVAILABLE
    }
    
    func isMovieSeatTaken() -> Bool {
        return type == SEAT_TYPE_TAKEN
    }
    
    func isMovieSeatRowTitle()-> Bool {
        return type == SEAT_TYPE_TEXT
    }
    
    init(title: String,type: String,price: Int,isSelected: Bool,seatNumber: String,id: Int) {
        self.title = title
        self.type = type
        self.price = price
        self.isSelect = isSelected
        self.seatNumber = seatNumber
        self.seatId = id
    }
    
    
}
