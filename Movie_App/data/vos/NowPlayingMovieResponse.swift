


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let nowPlayingMovieResopnse = try? newJSONDecoder().decode(NowPlayingMovieResopnse.self, from: jsonData)

import Foundation

// MARK: - NowPlayingMovieResopnse
struct NowPlayingMovieResopnse: Codable {
    let code: Int?
    let message: String?
    let data: [NowPlayingResult]?
}

// MARK: - Datum
struct NowPlayingResult: Codable {
    let id: Int?
    let originalTitle, releaseDate: String?
    let genres: [String]?
    let posterPath: String?

    enum CodingKeys: String, CodingKey {
        case id
        case originalTitle = "original_title"
        case releaseDate = "release_date"
        case genres
        case posterPath = "poster_path"
    }
}
