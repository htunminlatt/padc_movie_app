// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let paymentMethodResponse = try? newJSONDecoder().decode(PaymentMethodResponse.self, from: jsonData)

import Foundation

// MARK: - PaymentMethodResponse
struct PaymentMethodResponse: Codable {
    let code: Int?
    let message: String?
    let data: [PaymentResponseData]?
}

// MARK: - Datum
struct PaymentResponseData: Codable {
    let id: Int?
    let name, datumDescription: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case datumDescription = "description"
    }
}
