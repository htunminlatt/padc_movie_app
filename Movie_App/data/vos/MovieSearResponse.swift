// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let movieSearResponseResopnse = try? newJSONDecoder().decode(MovieSearResponseResopnse.self, from: jsonData)

import Foundation

// MARK: - MovieSearResponseResopnse
struct MovieSeatResponse: Codable {
    let code: Int?
    let message: String?
    let data: [[MovieSeatData]]?
}

// MARK: - Datum
struct MovieSeatData: Codable {
    let id: Int?
    let type: String?
    let seatName, symbol: String?
    let price: Int?

    enum CodingKeys: String, CodingKey {
        case id, type
        case seatName = "seat_name"
        case symbol, price
    }
    
    func convertToMovieSeatVO() -> MovieSeatVO {
        return MovieSeatVO(title: symbol ?? "", type: type ?? "", price: price ?? 0, isSelected: false, seatNumber: seatName ?? "", id: id ?? 0)
    }
}

enum TypeEnum: String, Codable {
    case available = "available"
    case space = "space"
    case text = "text"
}
