//
//  Multipart.swift
//  Movie_App
//
//  Created by Admin on 29/07/2021.
//

import Foundation
import UIKit

struct MultiPartFileData {
    let formfieldName: String
    let filename: String
    let data: Data
    let mineType: String
}
class MultiPart {
    static let shared = MultiPart()
    
    private init() {}
    
    func bindFormData(urlStr: String,params:[String: Any]?,fileData: [MultiPartFileData]?) -> (URLRequest,Data) {
        
        let boundary = "Boundary-\(UUID().uuidString)"
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "POST"
        let contentType = "multipart/form-data; boundary\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        let httpBody: Data? = createBody(withBoundary: boundary, parameters: params, filesData: fileData)
        
        return (request,httpBody ?? Data())
        
    }
    
    fileprivate func createBody(
        withBoundary boundary: String,
        parameters: [String: Any]?,
        filesData: [MultiPartFileData]?) -> Data {
        let httpBody = NSMutableData()
        
        //text
        if let parameters = parameters {
            for (parameterKey, parameterValue) in parameters {
                httpBody.append("--\(boundary)\r\n")
                httpBody.append("Content-Deposition: form-data; name=\"\(parameters)")
                httpBody.append("\r\n")
                httpBody.append("\(parameterValue)\r\n")

            }
        }
        
        //data
        if let lists = filesData {
            lists.forEach { (item) in
                httpBody.append("--\(boundary)\r\n")
                httpBody.append("Content-Deposition: form-data; name=\"\(item.formfieldName)")
                httpBody.append("Content-Type: \(item.mineType)\r\n")
                httpBody.append("\r\n")
                httpBody.append(item.data)
                httpBody.append("\r\n")

            }
        }
        
        //close boundary
        httpBody.append("--\(boundary)--")
        
        return httpBody as Data
        
    }
}

extension NSMutableData {
    func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}
