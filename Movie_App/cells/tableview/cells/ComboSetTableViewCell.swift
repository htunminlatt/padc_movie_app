//
//  ComboSetTableViewCell.swift
//  Movie_App
//
//  Created by MML on 15/06/2021.
//

import UIKit

struct ViewModel {
    var quantity: Int
    var foodPrice: Double
}

class ComboSetTableViewCell: UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var labelSnackDescription: UILabel!
    @IBOutlet weak var labelSnackName: UILabel!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var labelCount: UILabel!
    
    var count: Int = 0
    var snackPrice : Int = 0
    var decreasePrice : Int = 0
    
    var action: String = ""
    
    var onTapItem : ((Int,Int,Int,String)-> Void) = {_,_,_,_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.buttonMinus.tag = 0
        self.buttonPlus.tag = 1

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func onClickButton(_ sender: UIButton) {
       
        switch sender.tag {
        case 0:
            if self.count != 0 {
                self.action = "minus"
                self.snackPrice = self.snackData?.price ?? 0
                self.count -= 1
            }else {
                self.snackPrice = 0
            }
        case 1:
            self.action = "plus"
            self.count += 1
            self.snackPrice = self.snackData?.price ?? 0

        default:
            break
        }
        self.labelCount.text = "\(self.count)"
        
        onTapItem(self.snackPrice, self.snackData?.id ?? 0, self.count, self.action)
              
    }
    
    var snackData: SnackData? {
        didSet {
            if let snack = snackData {
                
                self.labelSnackName.text = snack.name
                self.labelSnackDescription.text = snack.datumDescription
                self.price.text = "\(snack.price ?? 0)$"
            }
        }
    }
    
}
