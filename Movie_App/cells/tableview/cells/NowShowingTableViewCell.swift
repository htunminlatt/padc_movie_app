//
//  NowShowingTableViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit


class NowShowingTableViewCell: UITableViewCell {

    @IBOutlet weak var nowShowingCollectionView: UICollectionView!
    
    var delegate: MovieDelegate? = nil
    
    var data: NowPlayingMovieResopnse? {
        didSet {
            if let _ = data {
                self.nowShowingCollectionView.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCollectionView()
    }
    
    private func registerCollectionView() {
        self.nowShowingCollectionView.delegate = self
        self.nowShowingCollectionView.dataSource = self
        self.nowShowingCollectionView.registerForCell(identifier: NowShowingCollectionViewCell.identifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}

extension NowShowingTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeCell(identifier: NowShowingCollectionViewCell.identifier, indexPath: indexPath)as! NowShowingCollectionViewCell
        cell.data = self.data?.data?[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapCell(id: self.data?.data?[indexPath.row].id ?? 0)
    }
    
}

