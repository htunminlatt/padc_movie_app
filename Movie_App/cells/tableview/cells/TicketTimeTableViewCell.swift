//
//  TicketTimeTableViewCell.swift
//  Movie_App
//
//  Created by Admin on 19/07/2021.
//

import UIKit

protocol getTimeSlotProtocal {
    func getId(id: Int,time: String,cinemaTitle: String)
}

class TicketTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var cinemaContentTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data: ResponseData?
    var delegate: getTimeSlotProtocal?
    var id: Int?
    var ticketTimeList : [TicketVO]? {
        didSet {
            self.collectionView.reloadData()
        }
        
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCollectionView()
        
    }

    private func registerCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.registerForCell(identifier: TicketTimeCollectionViewCell.identifier)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}

extension TicketTimeTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ticketTimeList?.count ?? 0
                
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeCell(identifier: TicketTimeCollectionViewCell.identifier, indexPath: indexPath)as! TicketTimeCollectionViewCell
        self.cinemaContentTitle.text = self.data?.cinema
        cell.data = self.ticketTimeList?[indexPath.row]
        
        cell.onTapItem = { [self] id in
            self.ticketTimeList?.forEach { (timelist) in
                if id == timelist.id {
                    timelist.isSelected = true
                    self.id = timelist.id

                    self.delegate?.getId(id: self.id ?? 0, time: timelist.time, cinemaTitle: self.data?.cinema ?? "")
                    }else {
                timelist.isSelected = false
                }

            }

            self.collectionView.reloadData()

        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width / 4.1, height: 40)
                    
    }
 
}
