//
//  PaymentMethodTableViewCell.swift
//  Movie_App
//
//  Created by MML on 15/06/2021.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCardDescription: UILabel!
    @IBOutlet weak var labelCardName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    var paymentData: PaymentResponseData? {
        didSet {
            if let data = paymentData {
                self.labelCardName.text = data.name
                self.labelCardDescription.text = data.datumDescription
            }
        }
    }
    
}
