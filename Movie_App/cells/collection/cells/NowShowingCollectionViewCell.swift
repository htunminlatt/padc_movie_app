//
//  NowShowingCollectionViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit
import SDWebImage

class NowShowingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: DesignableImageView!
    @IBOutlet weak var movieCellView: UIView!
    @IBOutlet weak var labelReleaseDate: UILabel!
    @IBOutlet weak var movieContentTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    var data: NowPlayingResult? {
        didSet {
            let url = URL(string: "\(Constants.baseImageUrl)\(data?.posterPath ?? "")")
            self.movieImageView.sd_setImage(with: url, placeholderImage: nil, options: .continueInBackground, context: nil)
            movieContentTitle.text = data?.originalTitle
            labelReleaseDate.text = data?.releaseDate
        }
    }
  
}
