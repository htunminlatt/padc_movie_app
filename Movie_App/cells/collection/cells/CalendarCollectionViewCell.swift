//
//  CalendarCollectionViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 01/02/2021.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelDayNumber: UILabel!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
    var didTapCell : ((Int)-> Void) = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapCell)))
    }
    
    @objc func onTapCell() {
        didTapCell(data!.dayNumber)
    }
    
    public var data: CalendarVO? = nil {
        didSet {
            if let calendar = data {
                self.labelDay.text = calendar.day
                self.labelDayNumber.text = "\(calendar.dayNumber)"
                
                (calendar.isSelected) ? (labelDayNumber.font = UIFont(name: "Avenir Book", size: 20.0)) : (labelDayNumber.font = UIFont(name: "Avenir Book", size: 12.0))
                (calendar.isSelected) ? (labelDay.textColor = .white) : (labelDay.textColor = .lightGray)
                (calendar.isSelected) ? (labelDayNumber.textColor = .white) : (labelDayNumber.textColor = .lightGray)
                
            }
        }
        
    }
    
    func didSetData(data: CalendarVO){
        self.data = data
    }

}
