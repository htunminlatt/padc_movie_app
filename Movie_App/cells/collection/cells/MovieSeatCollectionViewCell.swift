//
//  MovieSeatCollectionViewCell.swift
//  Movie_App
//
//  Created by MML on 01/06/2021.
//

import UIKit

class MovieSeatCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewContainerMovieSeat: UIView!
    @IBOutlet weak var lblMovieSeat: UILabel!
    
    var index: Int = 0
    
    var didTapCell : ((String,Int,String,Int)-> Void) = {_,_,_,_  in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

        func bindData(data: MovieSeatVO?) {
            if let movieSeatVO = data {
                if movieSeatVO.seatId == 1 || movieSeatVO.seatId == 14 {
                    self.lblMovieSeat.text = movieSeatVO.title
                }
                if movieSeatVO.isMovieSeatRowTitle() {
                    self.viewContainerMovieSeat.layer.cornerRadius = 0
                    self.viewContainerMovieSeat.backgroundColor = .white
                }else if movieSeatVO.isMovieSeatAvailable() {
                    self.viewContainerMovieSeat.clipsToBounds = true
                    self.viewContainerMovieSeat.layer.cornerRadius = 8
                    self.viewContainerMovieSeat.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
                    self.viewContainerMovieSeat.backgroundColor = UIColor(named: "available") ?? .lightGray
                                
                }else if movieSeatVO.isMovieSeatTaken() {
                    self.viewContainerMovieSeat.clipsToBounds = true
                    self.viewContainerMovieSeat.layer.cornerRadius = 8
                    self.viewContainerMovieSeat.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
                    self.viewContainerMovieSeat.backgroundColor = UIColor(named: "taken") ?? .darkGray
                }else {
                    self.viewContainerMovieSeat.layer.cornerRadius = 0
                    self.viewContainerMovieSeat.backgroundColor = .white

                }
            }
        }
    
    
    
}
