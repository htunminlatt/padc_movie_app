//
//  CreditCardCollectionViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 04/02/2021.
//

import UIKit


class CreditCardCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelCardNumber: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelCardHolder: UILabel!
    @IBOutlet weak var mainView: DesignableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.cornerRadius = 15
        self.mainView.layer.masksToBounds = true

    }
    
    var cardData: Card? {
        didSet {
            if let data = cardData {
                self.labelCardHolder.text = data.cardHolder
                self.labelDate.text = data.expirationDate
                self.labelCardNumber.text = data.cardNumber?.inserting(separator: "         ", every: 4)
            }
        }
    }

}
