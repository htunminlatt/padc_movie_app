//
//  CastCollectionViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 01/02/2021.
//

import UIKit
import SDWebImage

class CastCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backgroundImageView: DesignableImageView!
    @IBOutlet weak var labelContentTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundImageView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        backgroundImageView.layer.borderWidth = 0.3
        
    }
    
    var data: Cast? {
        didSet {
            if let data = data {
                
                if data.profilePath == nil || data.profilePath!.isEmpty {
                    self.labelContentTitle.text = data.name

                    self.backgroundImageView.isHidden = true
                }else {
                    self.labelContentTitle.text = ""
                    self.backgroundImageView.isHidden = false
                }
                
                let url = URL(string: "\(Constants.baseImageUrl)\(data.profilePath ?? "")")
                backgroundImageView.sd_setImage(with: url, placeholderImage: nil, options: .continueInBackground, context: nil)
//                labelContentTitle.text = data.name
            }
        }
    }
    

}
