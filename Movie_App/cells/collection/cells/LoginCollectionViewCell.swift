//
//  LoginCollectionViewCell.swift
//  Movie_App
//
//  Created by Admin on 17/07/2021.
//

import UIKit

class LoginCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setValueTextField(email: String,password: String) {
        self.textFieldEmail.text = email
        self.textFieldPassword.text = password
    }

}
