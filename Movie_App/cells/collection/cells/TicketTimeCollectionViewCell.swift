//
//  TicketTimeCollectionViewCell.swift
//  Movie_App
//
//  Created by Sai Xtun on 01/02/2021.
//

import UIKit

class TicketTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var selectedView: DesignableView!
    @IBOutlet weak var labelTime: UILabel!
    
    var onTapItem : ((Int)-> Void) = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedView.isUserInteractionEnabled = true
        self.selectedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
    }
    
    @objc func didTapView() {
        onTapItem(data?.id ?? 0)
    }
    
    public var data: TicketVO? = nil {
        didSet {
            if let time = data {
                self.labelTime.text = time.time
                (time.isSelected) ? (selectedView.backgroundColor = #colorLiteral(red: 0.3724273443, green: 0.1994216442, blue: 0.8558580279, alpha: 1)) : (selectedView.backgroundColor = .white)
                (time.isSelected) ? (labelTime.textColor = .white) : (labelTime.textColor = .black)
            }
        }
    }
    
   
}
