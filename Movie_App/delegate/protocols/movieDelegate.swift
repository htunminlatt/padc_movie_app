//
//  movieDelegate.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import Foundation

protocol MovieDelegate {
    func didTapCell(id: Int)
}
