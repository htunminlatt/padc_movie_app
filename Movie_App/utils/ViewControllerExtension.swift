//
//  ViewControllerExtension.swift
//  Movie_App
//
//  Created by Admin on 10/07/2021.
//

import Foundation
import UIKit
import NVActivityIndicatorView

let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: .cyan, padding: 0)


extension UIViewController {
   
 
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        loading.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loading)
        NSLayoutConstraint.activate([
            loading.widthAnchor.constraint(equalToConstant: 40),
            loading.heightAnchor.constraint(equalToConstant: 40),
            loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)])
        
        loading.startAnimating()
    }

    func hideloading() {
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "sorry!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { [weak self](success) in
            guard let self = self else {return}
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //get snackArray ....
    func getLastedSnackArray(snackList: [SnackData],tableView: UITableView,snacks: [snack]) -> [snack] {
       
        let snacklists = snackList
        var snackArray = snacks
        
        for i in 0..<snackList.count {
            let index = IndexPath(row: i, section: 0)
            let cell = tableView.cellForRow(at: index)as? ComboSetTableViewCell
            snackArray.append(snack(id: snacklists[i].id ?? 0, quantity: Int(cell?.labelCount.text ?? "") ?? 0))
        }
        
        for (index,value) in snackArray.enumerated().reversed() {
            if value.quantity == 0 {
                snackArray.remove(at: index)
            }
        }
        
        return snackArray
       
    }
    
}

