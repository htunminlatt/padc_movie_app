//
//  Router.swift
//  Movie_App
//
//  Created by Sai Xtun on 14/04/2021.
//

import Foundation
import UIKit

enum StoryboradName: String {
    case Main = "Main"
}

extension UIStoryboard {
    static func mainStoryboard() -> UIStoryboard {
        UIStoryboard(name: StoryboradName.Main.rawValue, bundle: nil)
    }
}

extension UIViewController {
    func navigateToMovieDetailViewcontroller(id: Int) {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: MovieDetailViewController.identifier)as? MovieDetailViewController  else {
            return
        }
        vc.movieId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToGetTicketViewController() {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: GetTicketViewController.identifier)as? GetTicketViewController  else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToMovieSeatViewController(id: Int,bookingDate: String,seatTitle: movieSeatTitle) {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: MovieSeatViewController.identifier)as? MovieSeatViewController  else {
            return
        }
        vc.bookingDate = bookingDate
        vc.timeSlotId = id
        vc.seatTitle = seatTitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToPayViewController(totalPrice: Int) {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: PayViewController.identifier)as? PayViewController  else {
            return
        }
        vc.priceTotal = totalPrice
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToConfirmPaymentViewController(totalPrice: Int,snacks: [snack]) {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: ConfirmPaymentViewController.identifier)as? ConfirmPaymentViewController  else {
            return
        }
        vc.totalPrice = totalPrice
        vc.snackArray = snacks
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToCreateTicketSuccessViewController(data: CheckOutSuccessResponse) {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: CreateTicketSuccessViewController.identifier)as? CreateTicketSuccessViewController  else {
            return
        }
        vc.checkOutData = data.data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nevigateToPromotionCodeViewController() {
        guard let vc = UIStoryboard.mainStoryboard().instantiateViewController(identifier: ProcomotionCodeViewController.identifier)as? ProcomotionCodeViewController  else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
