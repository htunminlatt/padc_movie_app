//
//  UserDefaults.swift
//  Movie_App
//
//  Created by Admin on 18/07/2021.
//

import Foundation
import UIKit
import SwiftyUserDefaults

extension DefaultsKeys {
    var token: DefaultsKey<String?> { .init("token") }
    var userName: DefaultsKey<String?> { .init("userName") }
    var email: DefaultsKey<String?> { .init("email") }
    
    
    var FbAccessToken: DefaultsKey<String?> { .init("FbAccessToken") }
    var Fbpassword: DefaultsKey<String?> { .init("Fbpassword") }
    var FbImageUrl: DefaultsKey<String?> { .init("FbImageUrl") }
    var FbEmail: DefaultsKey<String?> { .init("FbEmail") }

    var GoogleAccessToken: DefaultsKey<String?> { .init("GoogleAccessToken") }

    
}

