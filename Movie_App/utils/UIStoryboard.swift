//
//  UIStoryboard.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import Foundation
import UIKit

extension UIStoryboard{
    
    enum Storyboard : String{
        
        case Main = "Main"
        
        func instance(_ vc:String) -> UIViewController {
            return UIStoryboard(name: self.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier: vc)
        }
    }
    
    class func getStarted() -> UIViewController {
        return Storyboard.Main.instance(GetStartedViewController.className)
    }
    
    class func authLogin() -> UIViewController {
        return Storyboard.Main.instance(AuthViewController.className)
    }
    
    class func movieVc() -> UIViewController {
        return Storyboard.Main.instance(MovieViewController.className)
    }
    
    class func movieDetail() -> UIViewController {
        return Storyboard.Main.instance(MovieDetailViewController.className)
    }
    
    class func getTicket() -> UIViewController {
        return Storyboard.Main.instance(GetTicketViewController.className)
    }
    
    class func buyTicket() -> UIViewController {
        return Storyboard.Main.instance(BuyTicketViewController.className)
    }
    
    class func payPayment() -> UIViewController {
        return Storyboard.Main.instance(PayViewController.className)
    }
    
    class func confirmPayment() -> UIViewController {
        return Storyboard.Main.instance(ConfirmPaymentViewController.className)
    }
    
    class func ticketSuccess() -> UIViewController {
        return Storyboard.Main.instance(CreateTicketSuccessViewController.className)
    }
    
    class func promotionCode() -> UIViewController {
        return Storyboard.Main.instance(ProcomotionCodeViewController.className)
    }
    
}
