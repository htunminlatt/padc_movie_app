//
//  ViewExtension.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import Foundation
import UIKit

extension UIView {
    
    func addDashedBorder() {
            //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 2
            // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [10,10]

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),CGPoint(x: self.frame.width, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
        }
    
    
    func roundTopCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
    func roundButtonCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMaxYCorner]
    }
}

extension UITableViewCell {
   static var identifier: String {
        String(describing: self)
    }
}

extension UITableView {
    func registerForCell(identifier: String) {
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func dequeueCell<T: UITableViewCell>(identifier: String,indexPath: IndexPath)-> T {
        guard let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath)as? T else {
            return UITableViewCell() as! T
        }
        return cell
    }
    
}

extension UICollectionViewCell {
   static var identifier: String {
        String(describing: self)
    }
}

extension UICollectionView {
    func registerForCell(identifier: String) {
        register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    

    func dequeCell<T: UICollectionViewCell>(identifier: String,indexPath: IndexPath)-> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)as? T else {
            return UICollectionViewCell() as! T
        }
        return cell
    }
    
}


extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
    
}

extension UIViewController {
    static var identifier: String {
        String(describing: self)
    }
}
