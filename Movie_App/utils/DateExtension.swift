//
//  DateExtensio.swift
//  Movie_App
//
//  Created by Admin on 25/07/2021.
//

import Foundation
import UIKit

extension Date {
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }

    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    func getcurrentDateString(date: Date) -> String {
    
        let formatter = DateFormatter()
    
        let month = Calendar.current.component(.month, from: date)
    
        formatter.dateFormat = "yyy-\(month)-dd"

        let dateString = formatter.string(from: date)
    
        return dateString
    }
    
    
}
