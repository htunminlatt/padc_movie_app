//
//  GetTicketViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 01/02/2021.
//

import UIKit
import FSCalendar

class GetTicketViewController: UIViewController {
    @IBOutlet weak var mainTicketView: UIView!
   
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var button2D: UIButton!
    @IBOutlet weak var button3D: UIButton!
    @IBOutlet weak var buttonIMax: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!

    var movieTitle: String?
    var cinemaTitle: String?
    var cinemaDate: String?
    var cinemaTime: String?
    
    var date: String?
    private var networkAgent = MovieTimeImpl.shared
    
    private var timeSlotData : CinemaDayTimeSlotResopnse?
    
    var bookingDate: String?
    var currentYear: Int?
    var currentMonth: Int?
    var day: String?

    var monthString: String?
    
    var timeSlotId: Int = 0
    
    var formatter = DateFormatter()
    var screen = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTicketView.roundTopCorners(radius: 20.0)
        
        registerTableView()
        
        calendar.scrollDirection = .vertical
        calendar.scope = .week
        calendar.delegate = self
        calendar.dataSource = self
    
        let date = Date()
        self.bookingDate = date.getcurrentDateString(date: date)
        self.screen = "2D"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchCinemaTimeSlot(date: self.date ?? "")
        self.timeSlotId = 0

    }
    
    //MARK:-> register tableview
    private func registerTableView() {
        self.tableView.isScrollEnabled = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.separatorStyle = .none
        self.tableView.registerForCell(identifier: TicketTimeTableViewCell.identifier)
        self.tableView.tableFooterView = UIView()
        

    }
 
    @IBAction func onClickSelectAvailable(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.button2D.setTitleColor(.white, for: .normal)
            self.button3D.setTitleColor(.black, for: .normal)
            self.buttonIMax.setTitleColor(.black, for: .normal)
            
            self.button2D.backgroundColor = #colorLiteral(red: 0.3724273443, green: 0.1994216442, blue: 0.8558580279, alpha: 1)
            self.button3D.backgroundColor = .white
            self.buttonIMax.backgroundColor = .white
            
            self.screen = "2D"
        case 1:
            self.button2D.setTitleColor(.black, for: .normal)
            self.button3D.setTitleColor(.white, for: .normal)
            self.buttonIMax.setTitleColor(.black, for: .normal)
            
            self.button2D.backgroundColor = .white
            self.button3D.backgroundColor = #colorLiteral(red: 0.3724273443, green: 0.1994216442, blue: 0.8558580279, alpha: 1)
            self.buttonIMax.backgroundColor = .white
            self.screen = "3D"

        case 2:
            self.button2D.setTitleColor(.black, for: .normal)
            self.button3D.setTitleColor(.black, for: .normal)
            self.buttonIMax.setTitleColor(.white, for: .normal)
            
            self.button2D.backgroundColor = .white
            self.button3D.backgroundColor = .white
            self.buttonIMax.backgroundColor = #colorLiteral(red: 0.3724273443, green: 0.1994216442, blue: 0.8558580279, alpha: 1)
            self.screen = "IMAX"

        default:
            print("nil...")
        }
    }
    
    //MARK:-> apicall
    private func fetchCinemaTimeSlot(date: String) {
        self.showLoading()
        networkAgent.getCinemaTimeSlot(date: date) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()
                self.timeSlotData = data
                let count = data.data?.count ?? 0
                self.tableViewHeight.constant = CGFloat(count * 100)

                self.tableView.reloadData()
            case .fail(let error):
                self.hideloading()
                print(error)
            }
        }
        
    }
    
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickNext(_ sender: Any) {
        
        if self.timeSlotId == 0 {
            self.showAlert(message: "select time!")
        }else {
            let dateConcat = "\(self.bookingDate ?? ""), \(self.cinemaTime ?? "")"
            let title = movieSeatTitle(Movietitle: self.movieTitle ?? "", cinemaTitle: self.cinemaTitle ?? "", date: dateConcat)
            
            dict["screen"] = self.screen
            nevigateToMovieSeatViewController(id: self.timeSlotId , bookingDate: bookingDate ?? "", seatTitle: title)

        }
        
        
    }
    
}

extension GetTicketViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.timeSlotData?.data?.count ?? 0

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(identifier: TicketTimeTableViewCell.identifier, indexPath: indexPath)as? TicketTimeTableViewCell else {
            return UITableViewCell()
        }
               
        let resultData: [TicketVO]? = self.timeSlotData?.data?[indexPath.section].timeslots?.map {result -> TicketVO in
            return result.convertToTickVO()
        }
        
        resultData?.forEach({ (result) in
            if result.id == self.timeSlotId {
                result.isSelected = true
            }else {
                result.isSelected = false
            }
        })
        cell.collectionView.reloadData()
        
        cell.delegate = self
        cell.ticketTimeList = resultData
        cell.data = self.timeSlotData?.data?[indexPath.section]
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.timeSlotData?.data?[indexPath.section].timeslots?.count ?? 0 <= 3 {
            return 100

        }else {
            return 150

        }
    }
    
    
}

extension GetTicketViewController: getTimeSlotProtocal {
    func getId(id: Int, time: String,cinemaTitle: String) {
        self.timeSlotId = id
        self.cinemaTime = time
        self.cinemaTitle = cinemaTitle
        
        self.tableView.reloadData()
    }
  
}

extension GetTicketViewController: FSCalendarDelegate,FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        formatter.dateFormat = "dd MMM"
        let nameOfMonth = formatter.string(from: date)
        formatter.dateFormat = "EEE"
        let day = formatter.string(from: date)
        self.cinemaDate = "\(day), \(nameOfMonth)"

        self.bookingDate = date.getcurrentDateString(date: date)
        self.fetchCinemaTimeSlot(date: self.bookingDate ?? "")
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if date .compare(Date()) == .orderedSame {
            return false
        }
        else {
            return true
        }
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date
    {
       return Date()
    }
}
