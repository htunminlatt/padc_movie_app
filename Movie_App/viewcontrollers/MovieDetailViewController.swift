//
//  MovieDetailViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit
import SDWebImage

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var genreView: UIView!
    @IBOutlet weak var castsView: UIView!
    @IBOutlet weak var ratingView: RatingControl!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelImdb: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var labelMovieTitle: UILabel!
    @IBOutlet weak var labelMovieDetail: UILabel!
    @IBOutlet weak var movieTypeCollectionView: UICollectionView!
    @IBOutlet weak var castCollectionView: UICollectionView!
    @IBOutlet weak var movieDescriptionView: UIView!
    
    //MARK:-> variable
    var movieGenreArray = [String]()
    var networkAgent = MovieDetailImpl.shared
    var casts = [Cast]()
    private var movieTrailer = [MovieTrailer]()
    var movieTitle: String?

    var movieId : Int = -1
    var movieDate: String = ""
        
    private var movieDetail: MovieDetailResult?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundImageView.contentMode = .scaleAspectFill
        ratingView.starCount = 5
        self.movieDescriptionView.roundTopCorners(radius: 20.0)
        self.registerCollectionView()

        self.fetchMovieDetail(id: self.movieId)
        
        checkOutDict["movie_id"] = self.movieId
    }
    
    //MARK:-> init view
    func registerCollectionView() {
        self.castCollectionView.delegate = self
        self.castCollectionView.dataSource = self
        
        self.movieTypeCollectionView.delegate = self
        self.movieTypeCollectionView.dataSource = self
        
        self.movieTypeCollectionView.registerForCell(identifier: MovieTypeCollectionViewCell.identifier)
        self.castCollectionView.registerForCell(identifier: CastCollectionViewCell.identifier)
    }
    
    //MARK:-> apicall
    private func fetchMovieDetail(id: Int) {
        self.showLoading()
        
        networkAgent.getMovieDetail(id: id) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case .success(let data):
                self.bindData(data: data)
                self.castsView.isHidden = data.data?.casts?.count == 0
                self.casts = data.data?.casts ?? []
                
                self.movieDate = data.data?.releaseDate ?? ""
                self.movieDetail = data.data
                dict["movie_title"] = data.data?.originalTitle
                dict["movie_url"] = data.data?.posterPath
                
                self.castCollectionView.reloadData()
                self.movieTypeCollectionView.reloadData()
                
                self.hideloading()
            case .fail(let error):
                self.hideloading()
                print(error)
            }
        }
    }
    
//    //fetch movie trailer
//    private func fetchMovieTariler(id: Int){
//        networkAgent.getMovieTariler(id: id) { [weak self] (result) in
//
//            guard let self = self else { return }
//
//            switch result {
//            case .success(let data):
//                self.movieTrailer = data.results ?? []
//
//            case .fail(let error):
//                print(error)
//            }
//
//
//        }
//    }
    
    private func bindData(data: MovieDetailResopnse) {
        let url = URL(string: "\(Constants.baseImageUrl)\(data.data?.posterPath ?? "")")
        self.backgroundImageView.sd_setImage(with: url, placeholderImage: nil, options: .continueInBackground, context: nil)
        
        self.labelMovieTitle.text = data.data?.originalTitle
        let hour = Int((data.data?.runtime ?? 0) % 60)
        let minute = (data.data?.runtime ?? 0) - hour
        labelDuration.text = "\(hour) hr \(minute)"
        
       
        ratingView.rating = 4
        self.labelMovieDetail.text = data.data?.overview
        
//        data.data?.genres?.forEach({ (result) in
//            self.movieGenreArray.append(result. ?? "")
//        })
        
        data.data?.genres?.forEach({ (result) in
            self.movieGenreArray.append(result)
        })
        
        self.movieTitle = data.data?.originalTitle
        
        self.genreView.isHidden = self.movieGenreArray.count == 0
        
    }

    //MARK:-> actions
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickGetTicket(_ sender: Any) {
        if let vc =  UIStoryboard.getTicket() as? GetTicketViewController {
            vc.date = self.movieDate
            vc.movieTitle = self.movieDetail?.originalTitle
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func onclickPlayVideo(_ sender: Any) {
        let item = movieTrailer.first
        let youtubeKey = item?.key
        
        let playerVc = YoutubePlayerViewController()
        playerVc.youtubeKey = youtubeKey
        playerVc.movieTitle = self.movieTitle ?? ""
        self.navigationController?.pushViewController(playerVc, animated: true)
    }
    
  
}

extension MovieDetailViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == castCollectionView {
            return self.casts.count 

        }else {
            return self.movieGenreArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == castCollectionView {
            let cell = collectionView.dequeCell(identifier: CastCollectionViewCell.identifier, indexPath: indexPath) as! CastCollectionViewCell
            cell.data = self.casts[indexPath.row]
            return cell
            
        }else {
            let cell = collectionView.dequeCell(identifier: MovieTypeCollectionViewCell.identifier, indexPath: indexPath)as! MovieTypeCollectionViewCell
            cell.labelMovieType.text = self.movieGenreArray[indexPath.row]
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == castCollectionView {
            return CGSize(width: 85, height: collectionView.frame.height)
        }else {
            return CGSize(width: self.widthForString(text: self.movieGenreArray[indexPath.row], font: UIFont(name: "Geeza Pro", size: 14) ?? UIFont.systemFont(ofSize: 14)) + 20, height: 30.0)
        }
        
    }
    
    func widthForString(text: String,font: UIFont) -> CGFloat {
        let fontAttribute = [NSAttributedString.Key.font : font]
        let textSize = text.size(withAttributes: fontAttribute)
        return textSize.width
    }
    
}
