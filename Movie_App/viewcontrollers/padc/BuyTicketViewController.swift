//
//  BuyTicketViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 04/02/2021.
//

import UIKit

class BuyTicketViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickBuyTicket(_ sender: Any) {
        if let vc = UIStoryboard.payPayment() as? PayViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

}



////MARK:- SELECT SEAT
//    func selectedCellUpdate(_ seatdata: MovieSetVO) {
//        self.seatNumber.append(seatdata.seatNumber)
//        self.totalPrice += seatdata.price
//        self.lblTicketCount.text = "\(self.seatNumber.count)"
//
//        let string = self.seatNumber.joined(separator: ",")
//        self.lblSeatName.text = string
//        self.btnBuyTicket.setTitle("Buy Tickets For \(self.totalPrice)$", for: .normal)
//    }
//
//    //MARK:- DESELECT SEAT
//    func deSelectedCellUpdate(_ seatdata: MovieSetVO){
//        self.seatNumber = self.seatNumber.filter{ !$0.hasPrefix(seatdata.seatNumber)}
//        self.totalPrice -= seatdata.price
//        self.lblTicketCount.text = "\(self.seatNumber.count)"
//        
//        let string = self.seatNumber.joined(separator: ",")
//        self.lblSeatName.text = string
//        self.btnBuyTicket.setTitle("Buy Tickets For \(self.totalPrice)$", for: .normal)
//
//    }
