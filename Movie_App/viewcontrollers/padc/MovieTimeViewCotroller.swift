//
//  MovieTimeViewCotroller.swift
//  Movie_App
//
//  Created by MML on 31/05/2021.
//

import UIKit

class MovieTimeViewCotroller: UIViewController {

    @IBOutlet weak var collectionViewHeightWestPoint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightGoldenCity: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightAvalilableIn: NSLayoutConstraint!
    @IBOutlet weak var collectionViewWestPoint: UICollectionView!
    @IBOutlet weak var collectionViewGoldenCity: UICollectionView!
    @IBOutlet weak var collectionAvailableIn: UICollectionView!
    @IBOutlet weak var collectionViewDays: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.setUpHeightforCollectionView()
    }
    
    //setup collection view height
    fileprivate func setUpHeightforCollectionView() {
        self.collectionViewHeightAvalilableIn.constant = 56
        self.collectionViewHeightGoldenCity.constant = 56 * 2
        self.collectionViewHeightWestPoint.constant = 56 * 2
        
        self.view.layoutIfNeeded()

    }
    //register collection view
    fileprivate func registerCells() {
        collectionViewDays.delegate = self
        collectionViewDays.dataSource = self
        
        collectionAvailableIn.delegate = self
        collectionAvailableIn.dataSource = self
        
        collectionViewGoldenCity.delegate = self
        collectionViewGoldenCity.dataSource = self
        
        collectionViewWestPoint.delegate = self
        collectionViewWestPoint.dataSource = self
        
        collectionViewDays.registerForCell(identifier: CalendarCollectionViewCell.identifier)
        collectionViewWestPoint.registerForCell(identifier: TicketTimeCollectionViewCell.identifier)
        collectionAvailableIn.registerForCell(identifier: TicketTimeCollectionViewCell.identifier)
        collectionViewGoldenCity.registerForCell(identifier: TicketTimeCollectionViewCell.identifier)

    }

}

extension MovieTimeViewCotroller: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewDays {
            return 10
        }else if collectionView == collectionAvailableIn {
            return 3
        }else if collectionView == collectionViewGoldenCity {
            return 6
        }else{
            return 6
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewDays {
            guard let cell = collectionView.dequeCell(identifier: CalendarCollectionViewCell.identifier, indexPath: indexPath)as? CalendarCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        }else if collectionView == collectionAvailableIn {
            return collectionView.dequeCell(identifier: TicketTimeCollectionViewCell.identifier, indexPath: indexPath)
            
        }else if collectionView == collectionViewGoldenCity {
            return collectionView.dequeCell(identifier: TicketTimeCollectionViewCell.identifier, indexPath: indexPath)

        }else {
            return collectionView.dequeCell(identifier: TicketTimeCollectionViewCell.identifier, indexPath: indexPath)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionViewDays {
            return CGSize(width: 60, height: 80)
        }else {
            let width = collectionView.bounds.width / 3.1
            let height = CGFloat(48)
            
            return CGSize(width: width, height: height)
        }
    }
}
