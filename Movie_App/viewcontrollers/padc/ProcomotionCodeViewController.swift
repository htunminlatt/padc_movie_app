//
//  ProcomotionCodeViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 07/02/2021.
//

import UIKit
import FSCalendar

class ProcomotionCodeViewController: UIViewController,FSCalendarDelegate,FSCalendarDataSource {
    @IBOutlet weak var calendar: FSCalendar!
    
    var dateFormatter2 = DateFormatter()
    var cinemaDate = String()
    
    var startDate = Date()
    var endDate = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.scrollDirection = .vertical
        
        calendar.scope = .week
        calendar.gregorian.date(byAdding: .weekday, value: 7, to: calendar.currentPage)
//        calendar.firstWeekday = 2
//        if self.calendar.scope == .week {
//            startDate = self.calendar.currentPage
//            endDate = self.calendar.gregorian.date(byAdding: .weekday, value: 2, to: startDate)!
//        }
//        } else { // .month
//            let indexPath = self.calendar.calculator.indexPath(for: self.calendar.currentPage, scope: .month)
//            startDate = self.calendar.calculator.monthHead(forSection: (indexPath?.section)!)!
//            endDate = self.calendar.gregorian.date(byAdding: .day, value: 41, to: startDate)!
//        }

        calendar.delegate = self
        calendar.dataSource = self
//        let date = Date()
//
//        formatter.dateFormat = "dd MMM"
//        let nameOfMonth = formatter.string(from: date)
//        formatter.dateFormat = "EEE"
//        self.day = formatter.string(from: date)
//
//        self.cinemaDate = "\(self.day ?? ""), \(nameOfMonth)"
//        let month = Calendar.current.component(.month, from: date)
//
//        formatter.dateFormat = "yyy-\(month)-dd"

//        self.bookingDate = formatter.string(from: date)
        
    }
    
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if date .compare(Date()) == .orderedAscending {
            return false
        }
        else {
            return true
        }
    }
  
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateString = date.getcurrentDateString(date: date)
        
        print("your date....\(dateString)")
    }
    
   
    func minimumDate(for calendar: FSCalendar) -> Date
    {
       return Date()
    }
}



//        for i in 0..<self.snackList.count {
//            let index = IndexPath(row: i, section: 0)
//            let cell = self.comboSetTableView.cellForRow(at: index)as? ComboSetTableViewCell
//
//            snackArray.append(snack(id: self.snackList[i].id ?? 0, quantity: Int(cell?.labelCount.text ?? "") ?? 0))
//
//        }
//
//        for (index,value) in self.snackArray.enumerated().reversed() {
//            if value.quantity == 0 {
//                self.snackArray.remove(at: index)
//            }
//        }
//
//        checkOutVO.shared.snacks = self.snackArray
