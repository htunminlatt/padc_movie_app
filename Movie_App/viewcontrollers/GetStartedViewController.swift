//
//  GetStartedViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit
import SwiftyUserDefaults

class GetStartedViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        print("your token is...\(Defaults[\.token])")

    }
    @IBAction func onClickGetStarted(_ sender: Any) {
        if Defaults[\.token] == nil {
            if let vc = UIStoryboard.authLogin() as? AuthViewController {
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }else {
            if let vc = UIStoryboard.movieVc() as? MovieViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    

}
