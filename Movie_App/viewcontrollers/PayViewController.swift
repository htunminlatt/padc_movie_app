//
//  PayViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 04/02/2021.
//

import UIKit

class PayViewController: UIViewController {
    @IBOutlet weak var comboSetTableView: UITableView!
    @IBOutlet weak var paymentMethodTableView: UITableView!
    @IBOutlet weak var comboSetTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentMethodTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var buttonPay: DesignableButton!
    @IBOutlet weak var subTotal: UILabel!
    private var networkAgent = SnackModelImpl.shared
    
    private var snackList = [SnackData]()
    private var paymentData = [PaymentResponseData]()
    var totalAmount : Int = 0
    var totalDecreaseAmount : Int = 0
    var priceTotal : Int = 0
    
    var totalpriceamount = Int()

    var indexpath = [Int]()
    var snackArray = [snack]()
       
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableView()
        fetchSnackList()
        fetchPaymentData()
        self.buttonPay.setTitle("Pay \(self.priceTotal)$", for:.normal)

    }
  
    //MARK:-> initView
    private func registerTableView(){
        comboSetTableView.isScrollEnabled = false
        paymentMethodTableView.isScrollEnabled = false
        
        comboSetTableView.separatorStyle = .none
        paymentMethodTableView.separatorStyle = .none
        
        comboSetTableView.tableFooterView = UIView()
        paymentMethodTableView.tableFooterView = UIView()
        
        comboSetTableView.delegate = self
        paymentMethodTableView.delegate = self
        comboSetTableView.dataSource = self
        paymentMethodTableView.dataSource = self
        
        comboSetTableView.registerForCell(identifier: ComboSetTableViewCell.identifier)
        paymentMethodTableView.registerForCell(identifier: PaymentMethodTableViewCell.identifier)
    }
    
    
    //MARK:-> api call
    private func fetchSnackList() {
        self.showLoading()
        networkAgent.getSanckList {[weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()
                self.snackList = data.data ?? []
                self.comboSetTableViewHeight.constant = CGFloat(80 * self.snackList.count)

                self.comboSetTableView.reloadData()
            case .fail(let error):
                self.hideloading()
                print(error)
            }
        }
    }
    
    private func fetchPaymentData() {
        networkAgent.getPaymentMethod {[weak self] (result) in
            guard let self = self else {return}

            switch result {
            case .success(let data):
                self.paymentData = data.data ?? []
                self.paymentMethodTableViewHeight.constant = CGFloat(75 * self.paymentData.count)

                self.paymentMethodTableView.reloadData()
            case .fail(let error):
                print(error)
            }
        }
    }

    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickBuyTicket(_ sender: Any) {

        for (index,value) in snackArray.enumerated().reversed() {
            if value.quantity == 0 {
                snackArray.remove(at: index)
            }
        }
        
        let snacks = self.snackArray
            
        nevigateToConfirmPaymentViewController(totalPrice: self.totalpriceamount, snacks: snacks)
        
    }
    

}


extension PayViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.comboSetTableView {
            return self.snackList.count

        }else {
            return self.paymentData.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == comboSetTableView {
            let cell = tableView.dequeueCell(identifier: ComboSetTableViewCell.identifier, indexPath: indexPath) as! ComboSetTableViewCell
            cell.selectionStyle = .none
            cell.snackData = self.snackList[indexPath.row]
            
            cell.onTapItem = { amount,id,quantity,action in
                if action == "plus" {
                    self.totalAmount = self.totalAmount + amount
                }else {
                    self.totalAmount = self.totalAmount - amount
                }

                if let row = self.snackArray.firstIndex(where: {$0.id == id}) {
                    self.snackArray[row].quantity = quantity
                } else {
                    self.snackArray.append(snack(id: id, quantity: quantity))

                }
               
                self.comboSetTableView.reloadData()
                
            }
            self.totalpriceamount = self.priceTotal + self.totalAmount
            self.subTotal.text = "Sub Total : \(priceTotal + self.totalAmount)$"
            self.buttonPay.setTitle("Pay \(priceTotal + self.totalAmount)$", for:.normal)
                       
            return cell
        }else {
            guard let cell = tableView.dequeueCell(identifier: PaymentMethodTableViewCell.identifier, indexPath: indexPath)as? PaymentMethodTableViewCell else {
                return UITableViewCell()
            }
            cell.paymentData = self.paymentData[indexPath.row]
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == comboSetTableView {
            return 80
        }else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
