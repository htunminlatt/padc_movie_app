////
////  GoogleLogin.swift
////  Movie_App
////
////  Created by Admin on 24/07/2021.
////
//
//import Foundation
//import GoogleSignIn
//
//public class GoogleAuth : NSObject {
//
//private var onGoogleAuthFailed  : ((String)->Void)?
//private var onGoogleAuthSuccess : ((GoogleAuthProfileResponse)->Void)?
//
//public override init() {
//        super.init()
//    }
//
//public func start(view : UIViewController?, success: @escaping ((GoogleAuthProfileResponse)->Void), failure: @escaping ((String) -> Void) ) {
//        //Set params
//                    GIDSignIn.sharedInstance.del
//        GIDSignIn.sharedInstance().presentingViewController = view
//        GIDSignIn.sharedInstance().clientID = "115899705290-b8evneme5qchlhgdl18h2jgfi1kr83go.apps.googleusercontent.com" //Add your own OAuth Key
//
//        //Trigger Prompt
//        GIDSignIn.sharedInstance()?.signIn()
//
//        //Handle response
//        onGoogleAuthSuccess = { data in
//            success(data)
//        }
//
//        onGoogleAuthFailed = { data in
//            failure(data)
//        }
//
//    }
//
//    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//    if let error = error {
//    if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                onGoogleAuthFailed?("The user has not signed in before or they have since signed out.")
//              } else {
//                onGoogleAuthFailed?("\(error.localizedDescription)")
//              }
//    return
//            }
//            // Perform any operations on signed in user here.
//    let userId = user.userID ?? ""                // For client-side use only!
//    let token = user.authentication.idToken ?? ""
//    let fullName = user.profile.name ?? ""
//    let givenName = user.profile.givenName ?? ""
//    let familyName = user.profile.familyName ?? ""
//    let email = user.profile.email ?? ""
//
//    let userData = GoogleAuthProfileResponse(
//                id: userId, token: token, fullname: fullName,
//                giveName: givenName, familyName: familyName, email: email)
//
//            onGoogleAuthSuccess?(userData)
//
//        }
//
//    }
//
//    public struct GoogleAuthProfileResponse {
//    public let id, token, fullname, giveName, familyName, email : String
//    }
