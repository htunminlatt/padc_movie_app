//
//  AuthViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit
import SwiftyUserDefaults
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

class AuthViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var buttonGoogle: UIButton!
    @IBOutlet weak var buttonFaceBook: UIButton!
    @IBOutlet weak var labelSign: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var signInIndicatorView: UIView!
    @IBOutlet weak var loginIndicatorView: UIView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var authCollectionView: UICollectionView!
    
    var registerResponseData : RegisterDataResopnse?
    //MARK:-> property variables
    var networkAgent = MovieNetworkAgent.shared
    var token: String = ""
    
    var index: Int = 0
    var facebookTag : Int = 0
    
    var userName: String = ""
    var email: String = ""
 
    var loginTag: Int = 0
    
    private var fbAccessToken: String = ""
    private var fbImageUr: URL?
    
    private var signInConfig = GIDConfiguration.init(clientID: "115899705290-b8evneme5qchlhgdl18h2jgfi1kr83go.apps.googleusercontent.com")
    private var googleToken: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapView)))
        self.registerCollectionView()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
       self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
       if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
          return false
    }
          return true
    }
 
    //MARK: -> init view
    private func registerCollectionView() {
        self.buttonFaceBook.setTitle("Login With Facebook", for: .normal)
        self.buttonGoogle.setTitle("Login With Google", for: .normal)
        
        self.authCollectionView.isScrollEnabled = false
        self.authCollectionView.delegate = self
        self.authCollectionView.dataSource = self
        self.authCollectionView.registerForCell(identifier: LoginCollectionViewCell.identifier)
        self.authCollectionView.registerForCell(identifier: SignInCollectionViewCell.identifier)
        self.collectionViewHeight.constant = 260
        
        self.buttonFaceBook.addTarget(self, action: #selector(loginFacebook), for: .touchUpInside)
        self.buttonGoogle.addTarget(self, action: #selector(loginGoogle), for: .touchUpInside)
    }
    
    @objc func onTapView() {
        self.view.endEditing(true)
    }
    
    //MARK:-> click login , sign in

    @IBAction func onClickLogin_Sign(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.buttonFaceBook.setTitle("Login With Facebook", for: .normal)
            self.buttonGoogle.setTitle("Login With Google", for: .normal)

            self.index = 0
            self.loginLabel.textColor = #colorLiteral(red: 0.3724095225, green: 0.1995928288, blue: 0.8563470244, alpha: 1)
            self.loginIndicatorView.backgroundColor = #colorLiteral(red: 0.3724095225, green: 0.1995928288, blue: 0.8563470244, alpha: 1)
            
            self.signInIndicatorView.backgroundColor = .clear
            self.labelSign.textColor = .black
            self.collectionViewHeight.constant = 250

        case 1:
            self.buttonFaceBook.setTitle("SignIn With Facebook", for: .normal)
            self.buttonGoogle.setTitle("SignIn With Google", for: .normal)
            
            self.index = 1
            self.loginLabel.textColor = .black
            self.loginIndicatorView.backgroundColor = .clear
            
            self.signInIndicatorView.backgroundColor = #colorLiteral(red: 0.3724095225, green: 0.1995928288, blue: 0.8563470244, alpha: 1)
            self.labelSign.textColor = #colorLiteral(red: 0.3724095225, green: 0.1995928288, blue: 0.8563470244, alpha: 1)

            self.collectionViewHeight.constant = 436
          
        default:
            print("....")
        }
        
        self.authCollectionView.reloadData()

        
    }
    
    //MARK:-> register
 
    private func registerWithEmail(name: String,email: String,phone: String,password: String) {
        self.showLoading()
        networkAgent.registerWithEmail(name: name, email: email, phone: phone, password: password) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case .success(let data):
                self.hideloading()
                self.registerResponseData = data

                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.token] = self.registerResponseData?.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                                
            case .fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
            
        }
    }
    
    private func registerWithFacebook(name: String,email: String,phone: String,password: String,token: String) {
        self.showLoading()
        networkAgent.registerWithFacebook(name: name, email: email, phone: phone, password: password,fbaccessToken: token) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case .success(let data):
                self.hideloading()
                self.registerResponseData = data
                
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.FbAccessToken] = token
                    Defaults[\.token] = self.registerResponseData?.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case .fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
            
        }
    }
    
    private func registerWithGoogle(name: String,email: String,phone: String,password: String,token: String) {
        self.showLoading()
        networkAgent.registerWithGoogle(name: name, email: email, phone: phone, password: password,googleAccessToken: token) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case .success(let data):
                self.hideloading()
                self.registerResponseData = data
                
               
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.GoogleAccessToken] = token
                    Defaults[\.token] = self.registerResponseData?.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case .fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
            
        }
    }
    
    //MARK:-> login
    private func loginWithEmail(email: String,password: String) {
        self.showLoading()
        networkAgent.loginWithEmail(email: email, password: password) { [weak self] (result) in
            guard let self = self else {return}

            switch result {
            case.success(let data):
                self.hideloading()
                
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.token] = data.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case.fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
        }
    }
    
    private func loginWithFacebook(access_token: String) {
        self.showLoading()
        
        networkAgent.loginWithFacebook(access_token: access_token ) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()
                
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.token] = data.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case.fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
        }
    }
    
    private func loginWithGoogle(access_token: String) {
        self.showLoading()
        
        networkAgent.loginWithGoogle(access_token: access_token ) { [weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()
                
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    Defaults[\.token] = data.token

                    if let vc = UIStoryboard.movieVc() as? MovieViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case.fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)

            }
        }
    }
    
    //MARK:-> clickcomfirm
    @IBAction func onclickConfirm(_ sender: Any) {
        if self.index == 0 {
            let indexpath = IndexPath(row: 0, section: 0)
            let cell = self.authCollectionView.cellForItem(at: indexpath)as! LoginCollectionViewCell
                
            if cell.textFieldEmail.text == "" || cell.textFieldPassword.text == "" {
                self.showAlert(message: "email or password empty!")
            }else {
                self.loginWithEmail(email: cell.textFieldEmail.text ?? "", password: cell.textFieldPassword.text ?? "")
            }

        }else {
            let indexpath = IndexPath(row: 0, section: 0)
            let cell = self.authCollectionView.cellForItem(at: indexpath)as! SignInCollectionViewCell
            
            self.registerWithEmail(name: cell.textFieldName.text ?? "", email: cell.textFieldEmail.text ?? "", phone: cell.textFieldPhone.text ?? "", password: cell.Password.text ?? "")

        }
        
    }
    
    //MARK:-> login facebook , login google
    @objc func loginFacebook() {
        if self.index == 0 {
            if Defaults[\.FbAccessToken] == nil {
                self.showAlert(message: "No register account with Facebook")
            }else {
                self.loginWithFacebook(access_token: Defaults[\.FbAccessToken] ?? "")

            }
        }else {
            if self.fbAccessToken == "" {
                let facebookAuth = FacebookAuth()
                facebookAuth.start(vc: self) { (data) in
                    self.fbAccessToken = data.token
                    Defaults[\.FbImageUrl] = data.profilePic
                } failure: { (error) in
                    print(error)
                }
            }else {
                let indexpath = IndexPath(row: 0, section: 0)
                let cell = self.authCollectionView.cellForItem(at: indexpath)as! SignInCollectionViewCell
                
                self.registerWithFacebook(name: cell.textFieldName.text ?? "", email: cell.textFieldEmail.text ?? "", phone: cell.textFieldPhone.text ?? "", password: cell.Password.text ?? "", token: self.fbAccessToken)
            }
            
        }
    }
    
    @objc func loginGoogle() {
        if self.index == 0 {
            if Defaults[\.GoogleAccessToken] == nil {
                self.showAlert(message: "No register account with Google")
            }else {
                self.loginWithGoogle(access_token: Defaults[\.GoogleAccessToken] ?? "")
            }
        }else {
            if self.googleToken == "" {
                GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
                    guard error == nil else { return }
                    guard let user = user else { return }
                    self.googleToken = user.authentication.accessToken
                }
            }else {
                let indexpath = IndexPath(row: 0, section: 0)
                let cell = self.authCollectionView.cellForItem(at: indexpath)as! SignInCollectionViewCell
                
                self.registerWithGoogle(name: cell.textFieldName.text ?? "", email: cell.textFieldEmail.text ?? "", phone: cell.textFieldPhone.text ?? "", password: cell.Password.text ?? "", token: self.googleToken)
                
            }
            
        }
        

    }
    

}


//MARK:-> extension
extension AuthViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.index == 0 {
            guard let cell = collectionView.dequeCell(identifier: LoginCollectionViewCell.identifier, indexPath: indexPath)as? LoginCollectionViewCell else {
                return UICollectionViewCell()
            }
          
            return cell
        }else {
            let cell = collectionView.dequeCell(identifier: SignInCollectionViewCell.identifier, indexPath: indexPath) as! SignInCollectionViewCell
            
            cell.textFieldEmail.text = self.email
            cell.textFieldName.text = self.userName
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if index == 0 {
            return CGSize(width: authCollectionView.frame.width, height: 260)

        }else {
            return CGSize(width: authCollectionView.frame.width, height: 436)

        }

    }

    
}

