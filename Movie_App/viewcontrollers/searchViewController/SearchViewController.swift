//
//  SearchViewController.swift
//  Movie_App
//
//  Created by Admin on 11/07/2021.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    private var networkAgent = MovieNetworkAgent.shared
    
    private var data = [MovieResult]()
    var currentpage: Int = 1
    var totalPages: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCell()

        initView()
    }
    
    private func initView() {
        searchBar.searchTextField.textColor = .black
        searchBar.placeholder = "Search..."
        searchBar.delegate = self
        self.fetchSearchMovies(page: currentpage, keyword: searchBar.text ?? "")

    }
    
 
    private func registerCell(){
        searchCollectionView.delegate = self
        searchCollectionView.dataSource = self
        searchCollectionView.registerForCell(identifier: MovieCollectionViewCell.identifier)
        
    }
    
    //fetch searchmovie...
    private func fetchSearchMovies(page: Int,keyword: String) {
                
        networkAgent.getSearchMovie(query: keyword.encodeUrl() ?? "", page: page) { (result) in
            
            switch result {
            case .success(let data):
                self.data.append(contentsOf: data.results ?? [])
                self.totalPages = data.totalPages ?? 1
                self.searchCollectionView.reloadData()
            case .fail(let error):
                print(error)
            }
                                    
        }

    }

    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeCell(identifier: MovieCollectionViewCell.identifier, indexPath: indexPath)as? MovieCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.data = self.data[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.1 - 8, height: 260)

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let isLastRow = indexPath.row == self.data.count - 1
        let hasMorePage = currentpage < totalPages
        
        if isLastRow && hasMorePage {
            currentpage = currentpage + 1

            self.fetchSearchMovies(page: currentpage, keyword: searchBar.text ?? "")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigateToMovieDetailViewcontroller(id: self.data[indexPath.row].id ?? 0)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        
        if let data = searchBar.text {
            self.currentpage = 1
            self.totalPages = 1
            self.data.removeAll()
            
           
            self.fetchSearchMovies(page: currentpage, keyword: data)
            self.searchCollectionView.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
