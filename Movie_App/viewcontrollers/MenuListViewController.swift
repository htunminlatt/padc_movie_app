//
//  MenuListViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 05/02/2021.
//

import UIKit
import SideMenu
import SDWebImage
import SwiftyUserDefaults

class MenuListViewController: UIViewController {

    @IBOutlet weak var labelUserEmail: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var userImageView: DesignableImageView!
    
    var networkAgent = MovieNetworkAgent.shared
    
    override func viewDidLoad() {

        super.viewDidLoad()
        let url = URL(string: Defaults[\.FbImageUrl] ?? "")
        self.userImageView.sd_setImage(with: url, completed: nil)

        self.labelUserName.text = LoginVO.shared.username
        self.labelUserEmail.text = LoginVO.shared.email
    }
        
    @IBAction func onclickpromotion(_ sender: Any) {
        if let vc = UIStoryboard.promotionCode() as? ProcomotionCodeViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func logout() {
        networkAgent.logout { [weak self](result) in
            guard let self = self else {return}
            
            switch result {
            case .success(_):
                Defaults[\.token] = nil
                
                if let vc = UIStoryboard.authLogin() as? AuthViewController {
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            case .fail(let error):
                print(error)
            }
        }
    }
    
    @IBAction func onclicklLogout(_ sender: Any) {
        self.logout()
    }
    
    
}
