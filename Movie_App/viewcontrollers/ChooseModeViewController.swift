//
//  ChooseModeViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit

class ChooseModeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func onClickChooseMode(_ sender: UIButton) {
        switch sender.tag {
        case 0:
           if let vc = UIStoryboard.getStarted() as? GetStartedViewController {
            self.navigationController?.pushViewController(vc, animated: true)
            }
        case 1:
            print("dark mode...")
        default:
            print("//")
        }
    }
    

}
