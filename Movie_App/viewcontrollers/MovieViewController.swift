//
//  MovieViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 31/01/2021.
//

import UIKit
import SideMenu
import SDWebImage
import SwiftyUserDefaults

class MovieViewController: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet weak var movieTableView: UITableView!
    @IBOutlet weak var userImageView: DesignableImageView!
    @IBOutlet weak var labelUserName: UILabel!
    
    //:MARK-> variables
    var networkAgent = MovieModelImpl.shared
    var nowPlayingMoiveData : NowPlayingMovieResopnse?
    var coomingSoonMovieData: NowPlayingMovieResopnse?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerTableView()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        
        fetchNowPlayingMovie()
        fetchCoomingMovie()
        fetchProfile()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
       if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
          return true
    }
          return true
    }
      
    //:MARK-> initView
    private func registerTableView() {
        self.movieTableView.delegate = self
        self.movieTableView.dataSource = self
        self.movieTableView.separatorStyle = .none
        self.movieTableView.tableFooterView = UIView()
        self.movieTableView.registerForCell(identifier: NowShowingTableViewCell.identifier)
        self.movieTableView.registerForCell(identifier: CoomingSoonTableViewCell.identifier)

    }
    
  
    //:MARK-> apicall
    private func fetchNowPlayingMovie() {
        networkAgent.getNowPlayingMovie { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let data):
                self.nowPlayingMoiveData = data
                self.movieTableView.reloadSections(IndexSet(integer: 0), with: .automatic)

            case .fail(let error):
                print(error)
            }
        }
    }
    
    private func fetchCoomingMovie() {
        networkAgent.getCoomingSoonMovie { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let data):
                self.coomingSoonMovieData = data
                self.movieTableView.reloadSections(IndexSet(integer: 1), with: .automatic)

            case .fail(let error):
                print(error)
            }
        }
    }
    
    private func fetchProfile() {
        self.showLoading()

        networkAgent.fetchProfileData {[weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()

                // save user data
                userVO.shared.cardData = data.data?.cards ?? []
                let url = URL(string: Defaults[\.FbImageUrl] ?? "")
                self.userImageView.sd_setImage(with: url, completed: nil)
                self.labelUserName.text = data.data?.name
                
                Defaults[\.userName] = data.data?.name
                
                LoginVO.shared.email = data.data?.email ?? ""
                LoginVO.shared.username = data.data?.name ?? ""
                
            case .fail(let error):
                self.hideloading()
                print(error)
            }
        }
    }
    
    
    @IBAction func onclickSearch(_ sender: UIButton) {
        let vc = SearchViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


}

extension MovieViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case MovieType.NOWPLAYING_MOVIE.rawValue:
            let cell = tableView.dequeueCell(identifier: NowShowingTableViewCell.identifier, indexPath: indexPath)as! NowShowingTableViewCell
            cell.data = self.nowPlayingMoiveData
            cell.delegate = self
            
            return cell
        case MovieType.COOMINGSOON_MOVIE.rawValue:
            let cell = tableView.dequeueCell(identifier: CoomingSoonTableViewCell.identifier, indexPath: indexPath)as!  CoomingSoonTableViewCell
            cell.data = self.coomingSoonMovieData
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case MovieType.NOWPLAYING_MOVIE.rawValue:
            return tableView.frame.height / 2
        case MovieType.COOMINGSOON_MOVIE.rawValue:
            return tableView.frame.height / 2
        default:
            return 0
        }
    }
    
}

extension MovieViewController: MovieDelegate {
    func didTapCell(id: Int) {
       navigateToMovieDetailViewcontroller(id: id)
    }
    
    
}
