//
//  ConfirmPaymentViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 04/02/2021.
//

import UIKit
import HSCycleGalleryView

class ConfirmPaymentViewController: UIViewController {

    @IBOutlet weak var labelTotalPrice: UILabel!
    @IBOutlet weak var cardCollectionView: UICollectionView!
    @IBOutlet weak var textFieldCardNumber: UITextField!
    @IBOutlet weak var textFieldCardholder: UITextField!
    @IBOutlet weak var textFieldExpireDate: UITextField!
    @IBOutlet weak var textFieldCVC: UITextField!
    
    var totalPrice : Int = 0
    var networkAgent = CheckOutModelImpl.shared
    var snackArray = [snack]()
    
    private var cardData = userVO.shared.cardData
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.labelTotalPrice.text = "$ \(totalPrice)"
        self.registerCollectionView()
        self.bindCardData(index: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cardData = userVO.shared.cardData
        
        self.bindCardData(index: self.currentPage)
        
        if self.cardData.count == 0 {
            
        }else {
//            dict["id"] = self.cardData[currentPage].id ?? 0
            checkOutDict["card_id"] = self.cardData[currentPage].id ?? 0

        }

        self.cardCollectionView.reloadData()
    }
    
    //MARK:-> register colleview
   fileprivate func registerCollectionView() {
        self.cardCollectionView.delegate = self
        self.cardCollectionView.dataSource = self
        
        //self.cardCollectionView?.isPagingEnabled = true
        self.cardCollectionView.registerForCell(identifier: CreditCardCollectionViewCell.identifier)
       
        let flowlayout = UPCarouselFlowLayout()
        flowlayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: cardCollectionView.frame.size.height + 20)
        flowlayout.scrollDirection = .horizontal
        flowlayout.sideItemScale = 0.8
        flowlayout.sideItemAlpha = 1.0
        flowlayout.spacingMode = .fixed(spacing: 5.0)
        cardCollectionView.collectionViewLayout = flowlayout
    
        self.cardCollectionView.reloadData()
    }
    //MARK:-> bindData
    func bindCardData(index: Int) {
        if self.cardData.count == 0 {
            
        }else {
            self.textFieldCardNumber.text = self.cardData[index].cardNumber
            self.textFieldCardholder.text = self.cardData[index].cardHolder
            self.textFieldExpireDate.text = self.cardData[index].expirationDate
            self.textFieldCVC.text = self.cardData[index].cardType
            
        }

    }

    
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickConfirm(_ sender: Any) {
        self.checkOut()
    }
    @IBAction func onclickAddNewCard(_ sender: Any) {
        let vc = AddNewCardViewController()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            self.bindCardData(index: currentPage)
//            dict["id"] = self.cardData[currentPage].id ?? 0
            checkOutDict["card_id"] = self.cardData[currentPage].id ?? 0

        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = cardCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        }else {
            pageSize.height += layout.minimumLineSpacing
        }
        
        return pageSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = cardCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height

        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
    
}

extension ConfirmPaymentViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cardData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeCell(identifier: CreditCardCollectionViewCell.identifier, indexPath: indexPath) as? CreditCardCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.cardData = self.cardData[indexPath.row]
        return cell

    }
    
   
}

extension ConfirmPaymentViewController {
    func checkOut() {
        let timeSlotId = checkOutDict["timeSlotId"] as? Int
        let bookingDate = checkOutDict["bookingDate"] as? String
        let seatNumber = checkOutDict["seat_number"] as? String
        let movieId = checkOutDict["movie_id"] as? Int
        let cardId = checkOutDict["card_id"] as? Int
        
        self.showLoading()
        networkAgent.checkOut(movieId: movieId ?? 0, timeslot_id: timeSlotId ?? 0, seat_number: seatNumber ?? "", booking_date: bookingDate ?? "", card_id: cardId ?? 0, snackArray: self.snackArray) {[weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case .success(let data):
                self.hideloading()
                if data.code == 400 {
                    self.showAlert(message: data.message ?? "")
                }else {
                    self.nevigateToCreateTicketSuccessViewController(data: data)
                }
                
            case .fail(let error):
                self.hideloading()
                self.showAlert(message: error.debugDescription)
            }
        }

    }
}


