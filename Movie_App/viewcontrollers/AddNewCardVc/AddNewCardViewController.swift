//
//  AddNewCardViewController.swift
//  Movie_App
//
//  Created by Admin on 21/07/2021.
//

import UIKit
import SwiftyUserDefaults
class AddNewCardViewController: UIViewController {
    @IBOutlet weak var textFieldCardNumber: UITextField!
    @IBOutlet weak var textFieldCardholder: UITextField!
    @IBOutlet weak var textFieldExpireDate: UITextField!
    @IBOutlet weak var textFieldCVC: UITextField!
    
    private let networkAgent = MovieNetworkAgent.shared
    var datePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        createDatePicker()
    }
    
    //MARK:-> create datepicker
    private func createDatePicker() {
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 80))
               datePicker.addTarget(self, action: #selector(self.dateChanged), for: .allEvents)
        textFieldExpireDate.inputView = datePicker
               let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.datePickerDone))
               let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 40))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        textFieldExpireDate.inputAccessoryView = toolBar
    }
    
    @objc func datePickerDone() {
           textFieldExpireDate.resignFirstResponder()
    }

    @objc func dateChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let year: String = dateFormatter.string(from: self.datePicker.date)
        dateFormatter.dateFormat = "MM"
        let month: String = dateFormatter.string(from: self.datePicker.date)
        dateFormatter.dateFormat = "dd"
        let day: String = dateFormatter.string(from: self.datePicker.date)
        print("day...\(day)..month..\(month)")
        
        self.textFieldExpireDate.text = "\(day)/\(month)"
    }
 
    //MARK:-> apicall
    private func fetchProfile() {
        networkAgent.fetchProfileData {[weak self] (result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                userVO.shared.cardData = data.data?.cards ?? []
                
                self.navigationController?.popViewController(animated: true)

            case .fail(let error):
                print(error)
                self.hideloading()
            }
        }
    }
    
    private func createNewCard(cardNumber:String,cardHolder:String,date: String,cvc:String) {
        self.showLoading()
        networkAgent.createNewCard(cardNumber: cardNumber, cardHolder: cardHolder, date: date, cvc: cvc) { [weak self](result) in
            guard let self = self else {return}
            
            switch result {
            case.success(_):
                self.fetchProfile()
            case .fail(let error):
                print(error)
                self.hideloading()
            }
            
        }
    }
    
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickConfirm(_ sender: Any) {
        self.createNewCard(cardNumber: self.textFieldCardNumber.text ?? "", cardHolder: self.textFieldCardholder.text ?? "", date: self.textFieldExpireDate.text ?? "", cvc: self.textFieldCVC.text ?? "")
    }
    
}
