//
//  MovieSeatViewController.swift
//  Movie_App
//
//  Created by MML on 01/06/2021.
//

import UIKit

struct movieSeatTitle {
    var Movietitle: String
    var cinemaTitle: String
    var date: String
}

class MovieSeatViewController: UIViewController {

    @IBOutlet weak var labelCinameDate: UILabel!
    @IBOutlet weak var labelCinemaTitle: UILabel!
    @IBOutlet weak var labelMovieTitle: UILabel!
    @IBOutlet weak var btnBuyTicket: DesignableButton!
    @IBOutlet weak var labelSeatNumber: UILabel!
    @IBOutlet weak var labelTicketNumber: UILabel!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewMovieSeat: UICollectionView!
    
    var bookingDate = String()
    var timeSlotId = Int()
    
    private var networkAgent = MovieSeatImpl.shared
    
    var movieSeatdata: MovieSeatResponse?
    
    var seatData = [MovieSeatVO]()
    
    var totalPrice: Int = 0
    var seatCount = [String]()
    var seatTitle: movieSeatTitle?
    
    var seatNumber = [String]()
    
    override func viewDidLoad() {
    super.viewDidLoad()
        self.registerCells()
                
        fetchMovieSeat(bookingdate: self.bookingDate, id: self.timeSlotId)
        self.btnBuyTicket.setTitle("Buy Tickets For \(self.totalPrice)$", for: .normal)
                
        self.labelMovieTitle.text = seatTitle?.Movietitle
        self.labelCinemaTitle.text = seatTitle?.cinemaTitle
        self.labelCinameDate.text = seatTitle?.date
            
        dict["theater"] = seatTitle?.cinemaTitle
        
        checkOutDict["timeSlotId"] = self.timeSlotId
        checkOutDict["bookingDate"] = self.bookingDate

    }
    
    //MARK:-> init view
    fileprivate func registerCells() {
        self.collectionViewMovieSeat.allowsMultipleSelection = true
        self.collectionViewMovieSeat.delegate = self
        self.collectionViewMovieSeat.dataSource = self
        self.collectionViewMovieSeat.registerForCell(identifier: MovieSeatCollectionViewCell.identifier)
    }
    
    //MARK:-> apicall
    private func fetchMovieSeat(bookingdate: String,id: Int) {
        self.showLoading()
        self.networkAgent.getMovieSeat(bookingDate: bookingdate, id: id) { [weak self](result) in
            guard let self = self else {return}
            
            switch result {
            case.success(let data):
                self.hideloading()
                
                self.movieSeatdata = data
                
                self.movieSeatdata?.data?.forEach({ (result) in
                    result.forEach { (data) in
                        self.seatData.append(data.convertToMovieSeatVO())
                    }
                })
                    
                self.collectionViewHeight.constant = 30 * 14
                self.collectionViewMovieSeat.reloadData()
            case.fail(let error):
                self.hideloading()
                print(error)
            }
        }
    }

    @IBAction func onclickBuyTicket(_ sender: UIButton) {
        if self.seatNumber.count == 0 {
            self.showAlert(message: "Please select at least one seat!")
        }else {
            dict["seat_number"] = self.labelSeatNumber.text
            dict["seat_count"] = self.seatNumber.count
            
            checkOutDict["seat_number"] = self.labelSeatNumber.text
            
            nevigateToPayViewController(totalPrice: self.totalPrice)

        }

    }
    
    @IBAction func onclickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- SELECT SEAT
        func selectedCellUpdate(_ seatdata: MovieSeatVO) {
            self.seatNumber.append(seatdata.seatNumber)
            self.totalPrice += seatdata.price
            self.labelTicketNumber.text = "\(self.seatNumber.count)"
    
            let string = self.seatNumber.joined(separator: ",")
            self.labelSeatNumber.text = string
            self.btnBuyTicket.setTitle("Buy Tickets For \(self.totalPrice)$", for: .normal)
        }
    
        //MARK:- DESELECT SEAT
        func deSelectedCellUpdate(_ seatdata: MovieSeatVO){
            self.seatNumber = self.seatNumber.filter{ !$0.hasPrefix(seatdata.seatNumber)}
            self.totalPrice -= seatdata.price
            self.labelTicketNumber.text = "\(self.seatNumber.count)"
    
            let string = self.seatNumber.joined(separator: ",")
            self.labelSeatNumber.text = string
            self.btnBuyTicket.setTitle("Buy Tickets For \(self.totalPrice)$", for: .normal)
    
        }


}

extension MovieSeatViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.seatData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeCell(identifier: MovieSeatCollectionViewCell.identifier, indexPath: indexPath)as? MovieSeatCollectionViewCell  else {
            return UICollectionViewCell()
        }
       
        cell.bindData(data: self.seatData[indexPath.row])

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 14, height: 30)

    }
       
    
}

//MARK:- COLLECTION VIEW DELEGATE
extension MovieSeatViewController:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? MovieSeatCollectionViewCell
        let seatdata = seatData[indexPath.row]
        
        if seatdata.type == "available" {
            cell?.viewContainerMovieSeat.backgroundColor = #colorLiteral(red: 0.4245802462, green: 0.1904320121, blue: 0.9538946748, alpha: 1)
            selectedCellUpdate(seatdata)
        }else {
            print("nothing..")
        }
   
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? MovieSeatCollectionViewCell
        let seatdata = seatData[indexPath.row]
        if seatdata.type == "available" {
            cell?.viewContainerMovieSeat.backgroundColor = .lightGray
            deSelectedCellUpdate(seatdata)
        }else {
            print("nothing")
        }
    }
}

