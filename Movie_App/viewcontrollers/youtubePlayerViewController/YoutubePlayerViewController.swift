//
//  YoutubePlayerViewController.swift
//  Movie_App
//
//  Created by Admin on 11/07/2021.
//

import UIKit
import YouTubePlayer

class YoutubePlayerViewController: UIViewController {

    @IBOutlet var videoPlayer: YouTubePlayerView!
    @IBOutlet weak var contenTitle: UILabel!
    
    var youtubeKey : String? = nil
    var movieTitle: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contenTitle.text = movieTitle
        
        if let key = youtubeKey {
            let myVideoURL = URL(string: "https://www.youtube.com/watch?v=\(key)")
            videoPlayer.loadVideoURL(myVideoURL!)

        }else {
            //invalid id..
            print("invalid id....")
        }
    }

    @IBAction func onClickClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
