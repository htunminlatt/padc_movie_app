//
//  CreateTicketSuccessViewController.swift
//  Movie_App
//
//  Created by Sai Xtun on 04/02/2021.
//

import UIKit
import SDWebImage

class CreateTicketSuccessViewController: UIViewController {

    @IBOutlet weak var labelBookingNo: UILabel!
    @IBOutlet weak var ticketImageView: UIImageView!
    @IBOutlet weak var labelMovieTitle: UILabel!
    @IBOutlet weak var labelScreen: UILabel!
    @IBOutlet weak var labelShowTime: UILabel!
    @IBOutlet weak var labelTheater: UILabel!
    @IBOutlet weak var labelScreen2: UILabel!
    @IBOutlet weak var labelRow: UILabel!
    @IBOutlet weak var labelSeatCount: UILabel!
    @IBOutlet weak var labelTotalPrice: UILabel!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    private var networkAgent = MovieNetworkAgent.shared
    
    var checkOutData: CheckoutResponseData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ticketImageView.roundTopCorners(radius: 10.0)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.bindData()
    }
    
    func bindData() {
        let url = URL(string: "\(Constants.baseImageUrl)\(dict["movie_url"] ?? "")")
        self.ticketImageView.sd_setImage(with: url, completed: nil)
        
        let movieTitle = dict["movie_title"]
        let movieScreen = dict["screen"]
        let theater = dict["theater"]

        self.labelBookingNo.text = checkOutData?.bookingNo
        self.labelMovieTitle.text = movieTitle as? String
        self.labelScreen.text = movieScreen as? String
        self.labelShowTime.text = checkOutData?.timeslot?.startTime
        self.labelScreen2.text = movieScreen as? String
        self.labelRow.text = checkOutData?.row
        self.labelSeatCount.text = checkOutData?.seat
        self.labelTotalPrice.text = checkOutData?.total
        self.labelTheater.text = theater as? String
        
//        let qrUrl = URL(string: "\(Constants.baseImageUrl)\(checkOutData?.qrCode ?? "")")
//        self.qrCodeImageView.sd_setImage(with: qrUrl, completed: nil)
    }
    
  
    @IBAction func onclickClose(_ sender: Any) {
        if let vc = UIStoryboard.movieVc() as? MovieViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    

}
